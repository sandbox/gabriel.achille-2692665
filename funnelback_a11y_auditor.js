/**
 * Created by demonz on 24/03/2016.
 */
(function ($) {
    Drupal.behaviors.autoUpload = {
        attach: function (context, settings) {
            $('form', context).delegate('.field-widget-file-a11y-check input.form-file', 'change', function() {
                $(this).next('input[type="submit"]').mousedown();
            });
        }
    };
})(jQuery);
